# gitlab-webui-display-bug

Demonstrate a bug in gitlab's web ui.  Also see [bugreport](https://gitlab.com/gitlab-org/gitlab/issues/207499).

Directories with names that start with non-ascii (?) utf-8 (?) characters are
not displayed in web ui at all.  Furthermore they break the display, making the
columns *last commit* and *last update* unreadable.  If you cannot reproduce
this, here is a screenshot of what i looks like using chromium 79.0.3945.130

![screenshot](screenshot.png)

The git repository listed using `ls` in a terminal shows this:

```
drwxr-xr-x 2 * *   4096 Feb 21 15:12 Änderungen
-rw-r--r-- 1 * *    381 Feb 21 15:21 README.md
-rw-r--r-- 1 * * 128136 Feb 21 15:20 screenshot.png
```
